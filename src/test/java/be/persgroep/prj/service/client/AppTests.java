package be.persgroep.prj.service.client;

import be.persgroep.prj.pes.service.contexts.ArticleContentContext;
import be.persgroep.prj.pes.service.contexts.IClientContext;
import be.persgroep.prj.pes.service.enums.ContextKeyType;
import be.persgroep.prj.pes.service.enums.Domain;
import be.persgroep.prj.pes.service.service.ArticleService;
import junit.framework.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Locale;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("file:src/main/webapp/WEB-INF/applicationContext.xml")
public class AppTests {

    @Autowired
    private ArticleService articleService;

    @Qualifier(value = "peClientContext")
    @Autowired
    private IClientContext context;

    @Test
    public void test() throws Exception {
        context.addContextElement(ContextKeyType.DOMAIN, Domain.VKPLUS.getValue());
        context.addContextElement(ContextKeyType.LOCALE, Locale.ENGLISH.getLanguage());

        ArticleContentContext context = new ArticleContentContext();
        context.setMedia(true);

        System.out.print(articleService.getArticle(3359802, context).toString());
    }
}
